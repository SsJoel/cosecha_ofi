from rest_framework import serializers
from django.contrib.auth.models import User
from cosecha.models import AdmLotes, AdmTurnos, AdmCampos, AdmEtapas, AdmFundos, AdmUmbrales, Evaluadores, AdmCultivosVariedad, AdmLotesHileras, CosechaLotes, CosechaVigencias, Campana


class S_f_e_c_v(serializers.Serializer):
    id = serializers.IntegerField()
    descrip = serializers.CharField()
    e_id = serializers.IntegerField()
    e_descrip = serializers.CharField()
    c_id = serializers.IntegerField()
    c_descrip = serializers.CharField()
    v_id = serializers.IntegerField()
    v_descrip = serializers.CharField()
    dias = serializers.IntegerField()


class S_CosechaLotes(serializers.Serializer):
    id = serializers.IntegerField()
    alias = serializers.CharField()
    cod = serializers.CharField()
    fecha = serializers.CharField()
    hora = serializers.CharField()
    hi = serializers.CharField()
    hf = serializers.CharField()
    has_cosechada = serializers.CharField()
    descrip = serializers.CharField()
    num_hilera = serializers.CharField()
    latitud1 = serializers.CharField()
    longitud1 = serializers.CharField()
    latitud2 = serializers.CharField()
    longitud2 = serializers.CharField()
    dia = serializers.CharField()
    r = serializers.CharField()
    g = serializers.CharField()
    b = serializers.CharField()


class S_cosecha_sem(serializers.Serializer):
    id = serializers.IntegerField()
    fecha = serializers.CharField()
    hora = serializers.CharField()
    hi = serializers.IntegerField()
    hf = serializers.IntegerField()
    has_cosechada = serializers.CharField()
    id_bd = serializers.IntegerField()
    id_lotes_bd = serializers.IntegerField()
    id_adm_umbrales_bd = serializers.IntegerField()
    id_evaluadores_bd = serializers.IntegerField()
    id_adm_campana_bd = serializers.IntegerField()
    id_cosecha_vigencias_bd = serializers.IntegerField()
    fundo = serializers.CharField()
    lote = serializers.CharField()
    estado = serializers.CharField()


class S_api_etapas(serializers.ModelSerializer):
    class Meta:
        model = AdmEtapas
        fields = '__all__'


class S_api_fundos(serializers.ModelSerializer):
    etapas = S_api_etapas(many=True, read_only=True)

    class Meta:
        model = AdmFundos
        fields = ['id', 'descrip', 'etapas']
		

class S_api_lotes_h(serializers.ModelSerializer):
    class Meta:
        model = AdmLotesHileras
        fields = ['id', 'num_hilera', 'latitud1', 'longitud1', 'latitud2', 'longitud2', 'adm_cultivos_variedad_id', 'adm_lotes_id']


class S_api_lotes_l(serializers.ModelSerializer):
    hileras = S_api_lotes_h(many=True, read_only=True)
    class Meta:
        model = AdmLotes
        fields = ['id', 'cod', 'inicio_hilera', 'fin_hilera', 'has_sembrado', 'total_hileras', 'adm_turnos_id', 'hileras']


class S_api_lotes_t(serializers.ModelSerializer):
    lotes = S_api_lotes_l(many=True, read_only=True)
    class Meta:
        model = AdmTurnos
        fields = ['id', 'descrip', 'adm_campos_id', 'lotes']


class S_api_lotes_c(serializers.ModelSerializer):
    turnos = S_api_lotes_t(many=True, read_only=True)
    class Meta:
        model = AdmCampos
        fields = ['id', 'descrip', 'adm_etapas_id', 'turnos']


class S_api_lotes_e(serializers.ModelSerializer):
    campos = S_api_lotes_c(many=True, read_only=True)
    class Meta:
        model = AdmEtapas
        fields = ['id', 'descrip', 'campos']


class S_api_fe_e_all(serializers.ModelSerializer):
    class Meta:
        model = AdmEtapas
        fields = ['id', 'descrip', 'estado', 'adm_fundos_id']


class S_api_fe_f_all(serializers.ModelSerializer):
    etapas = S_api_fe_e_all(many=True, read_only=True)
    class Meta:
        model = AdmFundos
        fields = ['id', 'descrip', 'alias', 'estado', 'etapas']


class S_api_fundos_all(serializers.ModelSerializer):
    class Meta:
        model = AdmFundos
        fields = ['alias', 'descrip', 'id', 'estado']


class S_api_umbrales_all(serializers.ModelSerializer):
    class Meta:
        model = AdmUmbrales
        fields = ['id', 'r', 'g', 'b', 'n_dia', 'dia', 'id']


class S_api_evaluadores_all(serializers.ModelSerializer):
    class Meta:
        model = Evaluadores
        fields = '__all__'


class S_api_variedades_all(serializers.ModelSerializer):
    class Meta:
        model = AdmCultivosVariedad
        fields = '__all__'


class S_api_cosecha_lotes(serializers.ModelSerializer):
    class Meta:
        model = CosechaLotes
        fields = '__all__'
		

class S_api_lotes_h_b(serializers.ModelSerializer):
    class Meta:
        model = AdmLotesHileras
        fields = ['id', 'num_hilera', 'latitud1', 'longitud1', 'latitud2', 'longitud2', 'estado', 'adm_cultivos_variedad_id', 'adm_lotes_id']


class S_api_lotes_l_b(serializers.ModelSerializer):
    hileras = S_api_lotes_h_b(many=True, read_only=True)
    class Meta:
        model = AdmLotes
        fields = ['id', 'cod', 'inicio_hilera', 'fin_hilera', 'has_sembrado', 'total_hileras', 'estado', 'adm_turnos_id', 'hileras']


class S_api_lotes_t_b(serializers.ModelSerializer):
    lotes = S_api_lotes_l_b(many=True, read_only=True)
    class Meta:
        model = AdmTurnos
        fields = ['id', 'descrip', 'estado', 'adm_campos_id', 'lotes']


class S_api_lotes_c_b(serializers.ModelSerializer):
    turnos = S_api_lotes_t_b(many=True, read_only=True)
    class Meta:
        model = AdmCampos
        fields = ['id', 'descrip', 'estado', 'adm_etapas_id', 'turnos']


class S_api_lotes_cv(serializers.ModelSerializer):
    class Meta:
        model = CosechaVigencias
        fields = ['id', 'dias', 'estado', 'adm_campos_id', 'adm_cultivos_variedad_id']


class S_api_campana(serializers.ModelSerializer):
    class Meta:
        model = CosechaVigencias
        fields = ['id', 'cod']


