from django.db import models
from django.contrib.auth.models import User

class CategoriasEvaluadores(models.Model):
    nomb_categoria = models.CharField(max_length=50)
    inserted = models.DateTimeField()
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)

    class Meta:
        db_table = 'categorias_evaluadores'

    def __str__(self):
        return self.nomb_categoria


class AdmCultivos(models.Model):
    cod = models.CharField(max_length=10, blank=True, null=True)
    descrip = models.CharField(max_length=30, blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)

    class Meta:
        db_table = 'adm_cultivos'

    def __str__(self):
        return self.descrip


class Evaluadores(models.Model):
    nombres_eval = models.CharField(max_length=50)
    apellidos_eval = models.CharField(max_length=50)
    dni = models.CharField(max_length=15)
    adm_cultivos = models.ForeignKey(AdmCultivos, on_delete=models.CASCADE)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)
    auth_user = models.ForeignKey(User, on_delete=models.CASCADE)
    categorias_evaluadores = models.ForeignKey(CategoriasEvaluadores, on_delete=models.CASCADE)

    class Meta:
        db_table = 'evaluadores'

    def __str__(self):
        return self.nombres_eval + " " + self.apellidos_eval


class Campana(models.Model):
    cod = models.CharField(max_length=4, blank=True, null=True)
    fecha_i = models.DateField(blank=True, null=True)
    fecha_f = models.DateField(blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        db_table = 'adm_campana'


class AdmCultivosVariedad(models.Model):
    cod = models.CharField(max_length=10, blank=True, null=True)
    descrip = models.CharField(max_length=30, blank=True, null=True)
    adm_cultivos = models.ForeignKey(AdmCultivos, on_delete=models.CASCADE)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)

    class Meta:
        db_table = 'adm_cultivos_variedad'


class AdmFundos(models.Model):
    cod = models.CharField(max_length=20, blank=True, null=True)
    descrip = models.CharField(max_length=100, blank=True, null=True)
    cod_agv = models.CharField(max_length=3, blank=True, null=True)
    cod_adm_fundos = models.CharField(max_length=2, blank=True, null=True)
    alias = models.CharField(max_length=2, blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)

    class Meta:
        db_table = 'adm_fundos'


class AdmEtapas(models.Model):
    cod = models.CharField(max_length=20, blank=True, null=True)
    descrip = models.CharField(max_length=50, blank=True, null=True)
    adm_fundos = models.ForeignKey(AdmFundos, related_name="etapas", on_delete=models.CASCADE)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)

    class Meta:
        db_table = 'adm_etapas'

    def __str__(self):
        return self.descrip


class AdmCampos(models.Model):
    cod = models.CharField(max_length=20, blank=True, null=True)
    descrip = models.CharField(max_length=50, blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)
    adm_etapas = models.ForeignKey(AdmEtapas, related_name='campos', on_delete=models.CASCADE)
    adm_cultivos_variedad = models.ManyToManyField(AdmCultivosVariedad, through='CosechaVigencias')

    class Meta:
        db_table = 'adm_campos'

    def __str__(self):
        return self.descrip


class AdmTurnos(models.Model):
    cod = models.CharField(max_length=20, blank=True, null=True)
    descrip = models.CharField(max_length=50, blank=True, null=True)
    adm_campos = models.ForeignKey(AdmCampos, related_name="turnos", on_delete=models.CASCADE)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)

    class Meta:
        db_table = 'adm_turnos'

    def __str__(self):
        return self.descrip


class AdmLotes(models.Model):
    cod = models.CharField(max_length=25, blank=True, null=True)
    descrip = models.CharField(max_length=50, blank=True, null=True)
    has_sembrado = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    inicio_hilera = models.IntegerField(blank=True, null=True)
    fin_hilera = models.IntegerField(blank=True, null=True)
    total_hileras = models.IntegerField(blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)
    adm_turnos = models.ForeignKey(AdmTurnos, related_name="lotes", on_delete=models.CASCADE)
    evaluadores = models.ManyToManyField(Evaluadores, through='CosechaLotes')
    adm_cultivos_variedad = models.ManyToManyField(AdmCultivosVariedad, through='AdmLotesHileras')

    class Meta:
        db_table = 'adm_lotes'

    def __str__(self):
        return self.cod


class CosechaVigencias(models.Model):
    cod = models.CharField(max_length=2, blank=True, null=True)
    fecha_i = models.DateField(blank=True, null=True)
    fecha_f = models.DateField(blank=True, null=True)
    dias = models.IntegerField(blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True)
    adm_campos = models.ForeignKey(AdmCampos, on_delete=models.CASCADE)
    adm_cultivos_variedad = models.ForeignKey(AdmCultivosVariedad, on_delete=models.CASCADE)

    class Meta:
        db_table = 'cosecha_vigencias'


class AdmUmbrales(models.Model):
    cod = models.CharField(max_length=2, blank=True, null=True)
    r = models.IntegerField(blank=True, null=True)
    g = models.IntegerField(blank=True, null=True)
    b = models.IntegerField(blank=True, null=True)
    densidad = models.IntegerField(blank=True, null=True)
    n_dia = models.IntegerField(blank=True, null=True)
    dia = models.CharField(max_length=10, blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)

    class Meta:
        db_table = 'adm_umbrales'

    def __str__(self):
        return self.cod


class CosechaLotes(models.Model):
    fecha = models.DateField(blank=True, null=True)
    hora = models.TimeField(blank=True, null=True)
    hi = models.IntegerField(blank=True, null=True)
    hf = models.IntegerField(blank=True, null=True)
    has_cosechada = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    adm_umbrales = models.ForeignKey(AdmUmbrales, on_delete=models.CASCADE)
    adm_lotes = models.ForeignKey(AdmLotes, on_delete=models.CASCADE)
    adm_campana = models.ForeignKey(Campana, on_delete=models.CASCADE)
    cosecha_vigencias = models.ForeignKey(CosechaVigencias, on_delete=models.CASCADE)
    evaluadores = models.ForeignKey(Evaluadores, on_delete=models.CASCADE)
    registrado = models.DateTimeField(blank=True, null=True)
    modificado = models.DateTimeField(blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)

    class Meta:
        db_table = 'cosecha_lotes'

    def __str__(self):
        return self.adm_lotes


class AdmLotesHileras(models.Model):
    num_hilera = models.IntegerField(blank=True, null=True)
    latitud1 = models.DecimalField(max_digits=15, decimal_places=10, blank=True, null=True)
    longitud1 = models.DecimalField(max_digits=15, decimal_places=10, blank=True, null=True)
    latitud2 = models.DecimalField(max_digits=15, decimal_places=10, blank=True, null=True)
    longitud2 = models.DecimalField(max_digits=15, decimal_places=10, blank=True, null=True)
    plantas_hil = models.CharField(max_length=10, blank=True, null=True)
    plantas_has = models.CharField(max_length=10, blank=True, null=True)
    peso_hilera = models.CharField(max_length=5, blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)
    adm_cultivos_variedad = models.ForeignKey(AdmCultivosVariedad, on_delete=models.CASCADE)
    adm_lotes = models.ForeignKey(AdmLotes, related_name="hileras", on_delete=models.CASCADE)

    class Meta:
        db_table = 'adm_lotes_hileras'


'''
class CosechaHileras(models.Model):
    n_pasada = models.IntegerField(blank=True, null=True)
    adm_lotes_hileras = models.ForeignKey(AdmLotesHileras, on_delete=models.CASCADE)
    cosecha_lotes = models.ForeignKey(CosechaLotes, on_delete=models.CASCADE)
    estado = models.CharField(max_length=1, blank=True, null=True, default=0)

    class Meta:
        db_table = 'cosecha_hileras'
'''

