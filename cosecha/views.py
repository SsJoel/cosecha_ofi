from django.contrib.auth import login, logout
from django.http.response import JsonResponse
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status

import simplekml
import random
import os
import hashlib
import hmac
import datetime

from datetime import date, timedelta

from cosecha.serializers import S_f_e_c_v, S_api_fundos, S_api_etapas, S_api_lotes_e, S_api_fundos_all, \
    S_api_umbrales_all, S_api_evaluadores_all, S_api_variedades_all, S_api_lotes_c, S_api_cosecha_lotes, S_api_fe_f_all, \
    S_api_lotes_c_b, S_api_lotes_cv, S_api_campana, S_cosecha_sem
from cosecha.models import AdmEtapas, AdmFundos, CosechaLotes, AdmLotesHileras, AdmUmbrales, Evaluadores, \
    AdmCultivosVariedad, AdmCampos, CosechaVigencias, Campana
from rest_framework.decorators import api_view

from django.views.generic.edit import FormView
from .forms import FormularioLogin
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from rest_framework.authtoken.models import Token

from django.contrib.auth.models import User, Group


# _____________ Api ____________

@api_view(['GET'])
def api_fe(request):
    if request.method == 'GET':
        queryset = AdmFundos.objects.filter(estado__exact=0).prefetch_related('etapas')
        lotes_serial = S_api_fe_f_all(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_campana(request):
    if request.method == 'GET':
        queryset = Campana.objects.filter(estado__exact=0)
        lotes_serial = S_api_campana(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_fundos(request):
    if request.method == 'GET':
        queryset = AdmFundos.objects.filter(estado__exact=0)
        # print(queryset.query)
        lotes_serial = S_api_fundos_all(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_etapas2(request):
    if request.method == 'GET':
        queryset = AdmFundos.objects.filter(id__in=[1, 7]).prefetch_related('etapas')
        # print(queryset.query)
        lotes_serial = S_api_fundos(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_etapas(request):
    if request.method == 'GET':
        queryset = AdmEtapas.objects.filter(adm_fundos_id__in=[1, 7])
        # print(queryset.query)
        lotes_serial = S_api_etapas(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_lotes(request):
    if request.method == 'GET':
        id_etapa = request.GET.get('id_etapa', None)
        queryset = AdmEtapas.objects.filter(id__exact=id_etapa).prefetch_related('campos__turnos__lotes')
        print(queryset.query)
        lotes_serial = S_api_lotes_e(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_umbrales(request):
    if request.method == 'GET':
        queryset = AdmUmbrales.objects.filter(estado__exact=0)
        lotes_serial = S_api_umbrales_all(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_evaluadores(request):
    if request.method == 'GET':
        queryset = Evaluadores.objects.filter(estado__exact=0)
        lotes_serial = S_api_evaluadores_all(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_variedades(request):
    if request.method == 'GET':
        queryset = AdmCultivosVariedad.objects.filter(estado__exact=0)
        lotes_serial = S_api_variedades_all(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_ctlv(request):
    if request.method == 'GET':
        id_etapa = request.GET.get('id_etapa', None)
        queryset = AdmCampos.objects.filter(adm_etapas__exact=id_etapa, estado__exact=0).prefetch_related(
            'turnos__lotes__hileras')
        lotes_serial = S_api_lotes_c(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_ctlv_b(request):
    if request.method == 'GET':
        id_etapa = request.GET.get('id_etapa', None)
        queryset = AdmCampos.objects.filter(adm_etapas__exact=id_etapa, estado__exact=0).prefetch_related(
            'turnos__lotes__hileras')
        lotes_serial = S_api_lotes_c_b(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['POST'])
def api_cosecha_lotes(request):
    if request.method == 'POST':
        for datos in request.data:
            serializer = S_api_cosecha_lotes(data=datos)

            if serializer.is_valid():
                serializer.save()
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(True, status=status.HTTP_201_CREATED)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_covi(request):
    if request.method == 'GET':
        id_etapa = request.GET.get('id_etapa', None)
        queryset = CosechaVigencias.objects.filter(estado__exact=0,
                                                   adm_campos__adm_etapas__exact=id_etapa).prefetch_related(
            'adm_campos')
        lotes_serial = S_api_lotes_cv(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def api_cosecha_sem(request):
    if request.method == 'GET':
        id_etapa = request.GET.get('id_etapa', None)
        id_evaluador = request.GET.get('id_evaluador', None)
        fecha = request.GET.get('fecha', None)

        queryset = AdmFundos.objects.raw(
            'select cl.id, cl.fecha, cl.hora, cl.hi, cl.hf, cl.has_cosechada, cl.id as id_bd, cl.adm_lotes_id as id_lotes_bd, cl.adm_umbrales_id as id_adm_umbrales_bd, cl.evaluadores_id as id_evaluadores_bd, cl.adm_campana_id as id_adm_campana_bd, cl.cosecha_vigencias_id as id_cosecha_vigencias_bd, af.alias as fundo, al.cod as lote, cl.estado from cosecha_lotes cl inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos a on a.id = al.adm_turnos_id inner join adm_campos ac on ac.id = a.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id where cl.estado in (0, 2) and ae.id = %s and cl.evaluadores_id = %s and cl.fecha >= %s order by cl.fecha, al.cod;',
            [id_etapa, id_evaluador, fecha])
        lotes_serial = S_cosecha_sem(queryset, many=True)
        return JsonResponse(lotes_serial.data, safe=False)

    return Response({"message": "Hello, word!"})


# ______________ Web _____________


def home_view(request):
    print(request.user.first_name + " " + request.user.last_name + " " + request.user.username + " accedió al sistema.")
    str_Token = Token.objects.filter(user_id__exact=request.user.id)
    total = str_Token.count()

    if total > 0:
        str_Token.delete()
    tok = Token.objects.get_or_create(user=request.user)

    ingroup = User.objects.filter(pk=request.user.id, groups__name="Sanidad")

    if ingroup.count() > 0:
        grupo = 35
    else:
        grupo = 0

    d = datetime.datetime.now().day
    m = datetime.datetime.now().month
    a = datetime.datetime.now().year
    key = "86d20cd037d5bd84611248f47c3a021e1c1c1310"
    data = key + str(d) + str(m) + str(a)
    sdft = hmac.new(
        "Margito".encode('utf-8'),
        data.encode('utf-8'),
        hashlib.sha256
    ).hexdigest()
    return render(request, 'cosecha.html', {'api': sdft, 'spd': tok[0], 'group': grupo})


@api_view(['GET'])
def v_fundos(request):
    print(request.user.username + " consultó fundos.")
    if request.method == 'GET':
        api = request.GET['api']
        d = datetime.datetime.now().day
        m = datetime.datetime.now().month
        a = datetime.datetime.now().year
        key = "86d20cd037d5bd84611248f47c3a021e1c1c1310"
        data = key + str(d) + str(m) + str(a)
        sdft = hmac.new(
            "Margito".encode('utf-8'),
            data.encode('utf-8'),
            hashlib.sha256
        ).hexdigest()

        if api == sdft:
            queryset = AdmFundos.objects.raw(
                'select af.id as id, af.descrip as descrip, ae.id as e_id, ae.descrip as e_descrip, ac.id as c_id, ac.descrip as c_descrip, acv.id as v_id, acv.descrip as v_descrip, cv.dias from cosecha_vigencias cv inner join adm_cultivos_variedad acv on acv.id = cv.adm_cultivos_variedad_id inner join adm_campos ac on ac.id = cv.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id where cv.estado = 0 and acv.estado = 0 and ac.estado = 0 and ae.estado = 0 and af.estado = 0 order by af.id, ae.descrip, ac.descrip, acv.descrip;')
            lotes_serial = S_f_e_c_v(queryset, many=True)
            return JsonResponse(lotes_serial.data, safe=False)
        else:
            return Response([])

    return Response({"message": "Hello, word!"})


@api_view(['GET'])
def v_cosecha(request):
    if request.user.is_authenticated:
        print(request.user.username + " consultó cosecha.")
        if request.method == 'GET':
            api = request.GET['api']
            d = datetime.datetime.now().day
            m = datetime.datetime.now().month
            a = datetime.datetime.now().year
            key = "86d20cd037d5bd84611248f47c3a021e1c1c1310"
            data = key + str(d) + str(m) + str(a)
            sdft = hmac.new(
                "Margito".encode('utf-8'),
                data.encode('utf-8'),
                hashlib.sha256
            ).hexdigest()

            hora_f = datetime.datetime.now().strftime('%H%M%S')

            if api == sdft:
                f_id = int(request.GET['f_id'])
                e_id = int(request.GET['e_id'])
                c_id = int(request.GET['c_id'])
                v_id = int(request.GET['v_id'])
                fecha_i = datetime.datetime.strptime(str(request.GET['fecha_i']), '%Y-%m-%d')
                fecha_f = datetime.datetime.strptime(str(request.GET['fecha_f']), '%Y-%m-%d')

                if v_id != 0:
                    queryset = CosechaLotes.objects.raw(
                        'select cl.id, af.alias, al.cod, cl.fecha, cl.hora, cl.hi, cl.hf, cl.has_cosechada, acv.descrip, alh.num_hilera, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, au.n_dia, au.dia, au.r, au.g, au.b from cosecha_lotes cl inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos a on a.id = al.adm_turnos_id inner join adm_campos ac on ac.id = a.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_lotes_hileras alh on cl.adm_lotes_id = alh.adm_lotes_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join adm_umbrales au on au.id = cl.adm_umbrales_id where af.id = %s and ae.id = %s and ac.id = %s and acv.id = %s and cl.fecha BETWEEN %s and %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cl.estado = 0 order by 1, 10;',
                        [f_id, e_id, c_id, v_id, fecha_i, fecha_f]
                    )
                elif c_id != 0 and v_id == 0:
                    queryset = CosechaLotes.objects.raw(
                        'select cl.id, af.alias, al.cod, cl.fecha, cl.hora, cl.hi, cl.hf, cl.has_cosechada, acv.descrip, alh.num_hilera, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, au.n_dia, au.dia, au.r, au.g, au.b from cosecha_lotes cl inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos a on a.id = al.adm_turnos_id inner join adm_campos ac on ac.id = a.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_lotes_hileras alh on cl.adm_lotes_id = alh.adm_lotes_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join adm_umbrales au on au.id = cl.adm_umbrales_id where af.id = %s and ae.id = %s and ac.id = %s and acv.id != %s and cl.fecha BETWEEN %s and %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cl.estado = 0 order by 1, 10;',
                        [f_id, e_id, c_id, v_id, fecha_i, fecha_f]
                    )
                elif e_id != 0 and c_id == 0 and v_id == 0:
                    queryset = CosechaLotes.objects.raw(
                        'select cl.id, af.alias, al.cod, cl.fecha, cl.hora, cl.hi, cl.hf, cl.has_cosechada, acv.descrip, alh.num_hilera, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, au.n_dia, au.dia, au.r, au.g, au.b from cosecha_lotes cl inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos a on a.id = al.adm_turnos_id inner join adm_campos ac on ac.id = a.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_lotes_hileras alh on cl.adm_lotes_id = alh.adm_lotes_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join adm_umbrales au on au.id = cl.adm_umbrales_id where af.id = %s and ae.id = %s and ac.id != %s and acv.id != %s and cl.fecha BETWEEN %s and %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cl.estado = 0 order by 1, 10;',
                        [f_id, e_id, c_id, v_id, fecha_i, fecha_f]
                    )
                elif f_id != 0 and e_id == 0 and c_id == 0 and v_id == 0:
                    queryset = CosechaLotes.objects.raw(
                        'select cl.id, af.alias, al.cod, cl.fecha, cl.hora, cl.hi, cl.hf, cl.has_cosechada, acv.descrip, alh.num_hilera, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, au.n_dia, au.dia, au.r, au.g, au.b from cosecha_lotes cl inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos a on a.id = al.adm_turnos_id inner join adm_campos ac on ac.id = a.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_lotes_hileras alh on cl.adm_lotes_id = alh.adm_lotes_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join adm_umbrales au on au.id = cl.adm_umbrales_id where af.id = %s and ae.id != %s and ac.id != %s and acv.id != %s and cl.fecha BETWEEN %s and %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cl.estado = 0 order by 1, 10;',
                        [f_id, e_id, c_id, v_id, fecha_i, fecha_f]
                    )

                x, z, vr = 0, 0, ''
                parame = []
                lineas = []
                lu, ma, mi, ju, vi, sa, do, lmmjvsd, leyenda = 0, 0, 0, 0, 0, 0, 0, 0, []
                kml = simplekml.Kml()
                total = len(queryset)

                for p in queryset:
                    if x != 0:
                        if vr != str(p.id) + p.descrip:
                            hhi = parame[0][4]
                            hhf = parame[len(parame) - 1][4]
                            pol = kml.newpolygon(name='Agrovision')
                            pol.description = '' \
                                              '<!<html xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:msxsl="urn:schemas-microsoft-com:xslt">' \
                                              '<head>' \
                                              '<meta charset="UTF-8">' \
                                              '<title>Title</title>' \
                                              '</head>' \
                                              '<body style="margin:0px 0px 0px 0px;overflow:auto;background:#FFFFFF;height: auto">' \
                                              '<table style="font-family:Arial,Verdana,Times;font-size:15px;text-align:left;width:100%;height:50%;border-collapse:collapse;padding:2px">' \
                                              '<tr>' \
                                              '<td>Fundo: </td>' \
                                              '<td>' \
                                              + p.alias + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Lote: </td>' \
                                              '<td> ' \
                                              + cod + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Fecha: </td>' \
                                              '<td> ' \
                                              + fechaf + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Hora: </td>' \
                                              '<td> ' \
                                              + hora + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Dia: </td>' \
                                              '<td> ' \
                                              + dia + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Hilera: </td>' \
                                              '<td> Del ' \
                                              + str(hhi) + \
                                              ' al ' \
                                              + str(hhf) + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Ha Cosechada: </td>' \
                                              '<td> ' \
                                              + str(h_cosechado) + \
                                              ' ha</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Variedad: </td>' \
                                              '<td> ' \
                                              + variedad + \
                                              '</td>' \
                                              '</tr>' \
                                              '</table>' \
                                              '</td>' \
                                              '</tr>' \
                                              '</table>' \
                                              '</body>' \
                                              '</html>'

                            t_p = len(parame) * 2

                            for y in range(0, t_p):
                                if y >= len(parame):
                                    c = y - (z + 1) - z
                                    z += 1
                                    lineas.append((parame[c][3], parame[c][2]))

                                else:
                                    c = y
                                    z = 0
                                    lineas.append((parame[c][1], parame[c][0]))

                            lineas.append((parame[0][1], parame[0][0]))

                            pol.outerboundaryis = lineas
                            pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
                            pol.style.linestyle.width = 1
                            pol.style.polystyle.color = simplekml.Color.changealphaint(100,
                                                                                       simplekml.Color.rgb(r, g, b))

                            # print(fechaf + ' ' + cod + ' ' + hi + ' ' + hf)
                            parame = []
                            lineas = []

                            if n_dia == 1:
                                lu += h_cosechado
                            elif n_dia == 2:
                                ma += h_cosechado
                            elif n_dia == 3:
                                mi += h_cosechado
                            elif n_dia == 4:
                                ju += h_cosechado
                            elif n_dia == 5:
                                vi += h_cosechado
                            elif n_dia == 6:
                                sa += h_cosechado
                            elif n_dia == 7:
                                do += h_cosechado

                            lmmjvsd += h_cosechado

                    fechaf = str(datetime.datetime.strptime(str(p.fecha), '%Y-%m-%d').strftime('%d/%m/%Y'))
                    hora = str(p.hora)
                    cod = p.cod
                    hi = str(p.hi)
                    hf = str(p.hf)
                    dia = p.dia
                    n_dia = p.n_dia
                    variedad = p.descrip
                    h_cosechado = p.has_cosechada
                    r, g, b = p.r, p.g, p.b

                    parame.append([p.latitud1, p.longitud1, p.latitud2, p.longitud2, p.num_hilera])

                    x += 1
                    vr = str(p.id) + variedad

                    if x == total:
                        hhi = parame[0][4]
                        hhf = parame[len(parame) - 1][4]
                        pol = kml.newpolygon(name='Agrovision')
                        pol.description = '' \
                                          '<!<html xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:msxsl="urn:schemas-microsoft-com:xslt">' \
                                          '<head>' \
                                          '<meta charset="UTF-8">' \
                                          '<title>Title</title>' \
                                          '</head>' \
                                          '<body style="margin:0px 0px 0px 0px;overflow:auto;background:#FFFFFF;height: auto">' \
                                          '<table style="font-family:Arial,Verdana,Times;font-size:15px;text-align:left;width:100%;height:50%;border-collapse:collapse;padding:2px">' \
                                          '<tr>' \
                                          '<td>Fundo: </td>' \
                                          '<td>' \
                                          + p.alias + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Lote: </td>' \
                                          '<td> ' \
                                          + cod + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Fecha: </td>' \
                                          '<td> ' \
                                          + fechaf + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Hora: </td>' \
                                          '<td> ' \
                                          + hora + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Dia: </td>' \
                                          '<td> ' \
                                          + p.dia + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Hilera: </td>' \
                                          '<td> Del ' \
                                          + str(hhi) + \
                                          ' al ' \
                                          + str(hhf) + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Ha Cosechada: </td>' \
                                          '<td> ' \
                                          + str(h_cosechado) + \
                                          ' ha</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Variedad: </td>' \
                                          '<td> ' \
                                          + variedad + \
                                          '</td>' \
                                          '</tr>' \
                                          '</table>' \
                                          '</td>' \
                                          '</tr>' \
                                          '</table>' \
                                          '</body>' \
                                          '</html>'

                        t_p = len(parame) * 2

                        for y in range(0, t_p):
                            if y >= len(parame):
                                c = y - (z + 1) - z
                                z += 1
                                lineas.append((parame[c][3], parame[c][2]))

                            else:
                                c = y
                                z = 0
                                lineas.append((parame[c][1], parame[c][0]))

                        lineas.append((parame[0][1], parame[0][0]))

                        pol.outerboundaryis = lineas
                        pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
                        pol.style.linestyle.width = 1
                        pol.style.polystyle.color = simplekml.Color.changealphaint(100, simplekml.Color.rgb(r, g, b))

                        if n_dia == 1:
                            lu += h_cosechado
                        elif n_dia == 2:
                            ma += h_cosechado
                        elif n_dia == 3:
                            mi += h_cosechado
                        elif n_dia == 4:
                            ju += h_cosechado
                        elif n_dia == 5:
                            vi += h_cosechado
                        elif n_dia == 6:
                            sa += h_cosechado
                        elif n_dia == 7:
                            do += h_cosechado

                        lmmjvsd += h_cosechado

                if total > 0:
                    ff_hoy = str(date.today().year) + str(date.today().month) + str(date.today().day)
                    if not os.path.exists(
                            'static/datos/cosecha/' + str(f_id) + '/' + str(e_id) + '/' + str(c_id) + '/' + ff_hoy):
                        os.makedirs(
                            'static/datos/cosecha/' + str(f_id) + '/' + str(e_id) + '/' + str(c_id) + '/' + ff_hoy)

                    url = 'static/datos/cosecha/' + str(f_id) + '/' + str(e_id) + '/' + str(
                        c_id) + '/' + ff_hoy + '/kml_cosecha_' + ff_hoy + "_" + hora_f + '.kmz'
                    kml.savekmz(url, format=False)

                else:
                    url = 0

                return JsonResponse([url, [str(lu) + ' ha', str(ma) + ' ha', str(mi) + ' ha', str(ju) + ' ha',
                                           str(vi) + ' ha', str(sa) + ' ha', str(do) + ' ha',
                                           'Total: ' + str(lmmjvsd) + ' ha']], safe=False)
            else:
                return Response([])
    else:
        return Response([2])


@api_view(['GET'])
def v_pre_cosecha(request):
    if request.user.is_authenticated:
        print(request.user.username + " consultó pre cosecha.")
        if request.method == 'GET':
            api = request.GET['api']
            d = datetime.datetime.now().day
            m = datetime.datetime.now().month
            a = datetime.datetime.now().year
            key = "86d20cd037d5bd84611248f47c3a021e1c1c1310"
            data = key + str(d) + str(m) + str(a)
            sdft = hmac.new(
                "Margito".encode('utf-8'),
                data.encode('utf-8'),
                hashlib.sha256
            ).hexdigest()

            hora_f = datetime.datetime.now().strftime('%H%M%S')

            if api == sdft:
                f_id = int(request.GET['f_id'])
                e_id = int(request.GET['e_id'])
                c_id = int(request.GET['c_id'])
                v_id = int(request.GET['v_id'])
                v_dias = int(request.GET['v_dias'])
                fecha_i2 = datetime.datetime.strptime(str(request.GET['fechaf']), '%Y-%m-%d')
                fecha_i = datetime.datetime.strptime(str(request.GET['fechaf']), '%Y-%m-%d') - timedelta(days=1)

                if e_id == 0 and c_id == 0 and v_id == 0:
                    queryset = AdmLotesHileras.objects.raw(
                        'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id != %s and ac.id != %s and acv.id != %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado in (0, 2) group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by al.cod, alh.num_hilera;',
                        [f_id, e_id, c_id, v_id, fecha_i])
                elif e_id != 0 and c_id == 0 and v_id == 0:
                    queryset = AdmLotesHileras.objects.raw(
                        'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id = %s and ac.id != %s and acv.id != %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado in (0, 2) group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by al.cod, alh.num_hilera;',
                        [f_id, e_id, c_id, v_id, fecha_i])
                elif e_id != 0 and c_id != 0 and v_id == 0:
                    queryset = AdmLotesHileras.objects.raw(
                        'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id = %s and ac.id = %s and acv.id != %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado in (0, 2) group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by al.cod, alh.num_hilera;',
                        [f_id, e_id, c_id, v_id, fecha_i])
                elif e_id != 0 and c_id != 0 and v_id != 0:
                    queryset = AdmLotesHileras.objects.raw(
                        'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id = %s and ac.id = %s and acv.id = %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado in (0, 2) group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by al.cod, alh.num_hilera;',
                        [f_id, e_id, c_id, v_id, fecha_i])
                elif e_id == 0 and c_id == 0 and v_id != 0:
                    queryset = AdmLotesHileras.objects.raw(
                        'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id != %s and ac.id != %s and acv.id = %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado in (0, 2) group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by al.cod, alh.num_hilera;',
                        [f_id, e_id, c_id, v_id, fecha_i])
                elif e_id != 0 and c_id == 0 and v_id != 0:
                    queryset = AdmLotesHileras.objects.raw(
                        'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id = %s and ac.id != %s and acv.id = %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado in (0, 2) group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by al.cod, alh.num_hilera;',
                        [f_id, e_id, c_id, v_id, fecha_i])

                x, z, vr = 0, 0, ''
                parame = []
                lineas = []
                solo_lineas = []
                a8, a7, a6, a5, a4, a3, a2, a1, xx, b1, b2, b3, b4, tot_ley = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
                kml = simplekml.Kml()
                r = random.randrange(0, 255)
                g = random.randrange(0, 255)
                b = random.randrange(0, 255)

                cds = [[148, 128, 172], [255, 159, 159], [255, 216, 159], [255, 255, 150], [56, 168, 0], [255, 255, 0],
                       [255, 150, 0], [255, 0, 0], [40, 0, 90]]
                total = len(queryset)

                for p in queryset:
                    if x != 0:
                        if vr != p.cod + str(p.fecha) + str(p.descrip):
                            pol = kml.newpolygon(name='Agrovision')

                            if len(parame) == 1:
                                solo_lineas.append([fechaf, cod, n_hilera])

                            t_p = len(parame) * 2

                            p_hi = parame[0][4]
                            p_hf = parame[len(parame) - 1][4]

                            p_hc = round(((p_hf - p_hi + 1) * has_sembrado) / tot_hileras, 2)
                            dias_rest = (fecha_i2 - fechasf).days

                            for y in range(0, t_p):
                                if y >= len(parame):
                                    c = y - (z + 1) - z
                                    z += 1
                                    lineas.append((parame[c][3], parame[c][2]))

                                else:
                                    c = y
                                    z = 0
                                    lineas.append((parame[c][1], parame[c][0]))

                            lineas.append((parame[0][1], parame[0][0]))

                            if dias_rest == (int(optimo) - 4):
                                a5 += p_hc
                            elif dias_rest == (int(optimo) - 5):
                                a6 += p_hc
                            elif dias_rest == (int(optimo) - 6):
                                a7 += p_hc
                            elif dias_rest <= (int(optimo) - 7):
                                a8 += p_hc

                            if dias_rest <= (int(optimo) - 4):
                                r = cds[0][0]
                                g = cds[0][1]
                                b = cds[0][2]
                                a4 += p_hc

                            elif dias_rest == (int(optimo) - 3):
                                r = cds[1][0]
                                g = cds[1][1]
                                b = cds[1][2]
                                a3 += p_hc

                            elif dias_rest == (int(optimo) - 2):
                                r = cds[2][0]
                                g = cds[2][1]
                                b = cds[2][2]
                                a2 += p_hc

                            elif dias_rest == (int(optimo) - 1):
                                r = cds[3][0]
                                g = cds[3][1]
                                b = cds[3][2]
                                a1 += p_hc

                            elif dias_rest == int(optimo):
                                r = cds[4][0]
                                g = cds[4][1]
                                b = cds[4][2]
                                xx += p_hc

                            elif dias_rest == (int(optimo) + 1):
                                r = cds[5][0]
                                g = cds[5][1]
                                b = cds[5][2]
                                b1 += p_hc

                            elif dias_rest == (int(optimo) + 2):
                                r = cds[6][0]
                                g = cds[6][1]
                                b = cds[6][2]
                                b2 += p_hc

                            elif dias_rest == (int(optimo) + 3):
                                r = cds[7][0]
                                g = cds[7][1]
                                b = cds[7][2]
                                b3 += p_hc

                            elif dias_rest >= (int(optimo) + 4):
                                r = cds[8][0]
                                g = cds[8][1]
                                b = cds[8][2]
                                b4 += p_hc

                            tot_ley += p_hc

                            pol.description = '' \
                                              '<!<html xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:msxsl="urn:schemas-microsoft-com:xslt">' \
                                              '<head>' \
                                              '<meta charset="UTF-8">' \
                                              '<title>Title</title>' \
                                              '</head>' \
                                              '<body style="margin:0px 0px 0px 0px;overflow:auto;background:#FFFFFF;height: auto">' \
                                              '<table style="font-family:Arial,Verdana,Times;font-size:15px;text-align:left;width:100%;height:50%;border-collapse:collapse;padding:2px">' \
                                              '<tr>' \
                                              '<td>Fundo: </td>' \
                                              '<td>' \
                                              + p.alias + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Lote: </td>' \
                                              '<td> ' \
                                              + cod + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Fecha: </td>' \
                                              '<td> ' \
                                              + fechaf + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Hilera: </td>' \
                                              '<td> Del ' \
                                              + str(p_hi) + \
                                              ' al ' \
                                              + str(p_hf) + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Ha Cosechada: </td>' \
                                              '<td> ' \
                                              + str(p_hc) + \
                                              ' ha</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Variedad: </td>' \
                                              '<td> ' \
                                              + variedad + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Optimo: </td>' \
                                              '<td> ' \
                                              + str(optimo) + ' d' + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Real: </td>' \
                                              '<td> ' \
                                              + str(dias_rest) + ' d' + \
                                              '</td>' \
                                              '</tr>' \
                                              '</table>' \
                                              '</td>' \
                                              '</tr>' \
                                              '</table>' \
                                              '</body>' \
                                              '</html>'

                            pol.outerboundaryis = lineas
                            pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
                            pol.style.linestyle.width = 1
                            pol.style.polystyle.color = simplekml.Color.changealphaint(100,
                                                                                       simplekml.Color.rgb(r, g, b))

                            # print(fechaf + ' ' + cod + ' ' + hi + ' ' + hf)
                            parame = []
                            lineas = []

                    fechasf = datetime.datetime.strptime(str(p.fecha), '%Y-%m-%d')
                    fechaf = str(fechasf.strftime('%d/%m/%Y'))
                    cod = p.cod
                    variedad = p.descrip
                    has_sembrado = p.has_sembrado
                    tot_hileras = p.total_hileras
                    pasada = p.n
                    optimo = p.dias
                    n_hilera = p.num_hilera

                    parame.append([p.latitud1, p.longitud1, p.latitud2, p.longitud2, p.num_hilera])

                    x += 1
                    vr = p.cod + str(p.fecha) + str(p.descrip)

                    if (x == total):
                        pol = kml.newpolygon(name='Agrovision')

                        if len(parame) == 1:
                            solo_lineas.append([fechaf, cod, n_hilera])

                        t_p = len(parame) * 2

                        p_hi = parame[0][4]
                        p_hf = parame[len(parame) - 1][4]

                        p_hc = round(((p_hf - p_hi + 1) * has_sembrado) / tot_hileras, 2)
                        dias_rest = (fecha_i2 - fechasf).days

                        for y in range(0, t_p):
                            if y >= len(parame):
                                c = y - (z + 1) - z
                                z += 1
                                lineas.append((parame[c][3], parame[c][2]))

                            else:
                                c = y
                                z = 0
                                lineas.append((parame[c][1], parame[c][0]))

                        lineas.append((parame[0][1], parame[0][0]))

                        if dias_rest == (int(optimo) - 4):
                            a5 += p_hc
                        elif dias_rest == (int(optimo) - 5):
                            a6 += p_hc
                        elif dias_rest == (int(optimo) - 6):
                            a7 += p_hc
                        elif dias_rest <= (int(optimo) - 7):
                            a8 += p_hc

                        if dias_rest <= (int(optimo) - 4):
                            r = cds[0][0]
                            g = cds[0][1]
                            b = cds[0][2]
                            a4 += p_hc

                        elif dias_rest == (int(optimo) - 3):
                            r = cds[1][0]
                            g = cds[1][1]
                            b = cds[1][2]
                            a3 += p_hc

                        elif dias_rest == (int(optimo) - 2):
                            r = cds[2][0]
                            g = cds[2][1]
                            b = cds[2][2]
                            a2 += p_hc

                        elif dias_rest == (int(optimo) - 1):
                            r = cds[3][0]
                            g = cds[3][1]
                            b = cds[3][2]
                            a1 += p_hc

                        elif dias_rest == int(optimo):
                            r = cds[4][0]
                            g = cds[4][1]
                            b = cds[4][2]
                            xx += p_hc

                        elif dias_rest == (int(optimo) + 1):
                            r = cds[5][0]
                            g = cds[5][1]
                            b = cds[5][2]
                            b1 += p_hc

                        elif dias_rest == (int(optimo) + 2):
                            r = cds[6][0]
                            g = cds[6][1]
                            b = cds[6][2]
                            b2 += p_hc

                        elif dias_rest == (int(optimo) + 3):
                            r = cds[7][0]
                            g = cds[7][1]
                            b = cds[7][2]
                            b3 += p_hc

                        elif dias_rest >= (int(optimo) + 4):
                            r = cds[8][0]
                            g = cds[8][1]
                            b = cds[8][2]
                            b4 += p_hc

                        tot_ley += p_hc

                        pol.description = '' \
                                          '<!<html xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:msxsl="urn:schemas-microsoft-com:xslt">' \
                                          '<head>' \
                                          '<meta charset="UTF-8">' \
                                          '<title>Title</title>' \
                                          '</head>' \
                                          '<body style="margin:0px 0px 0px 0px;overflow:auto;background:#FFFFFF;height: auto">' \
                                          '<table style="font-family:Arial,Verdana,Times;font-size:15px;text-align:left;width:100%;height:50%;border-collapse:collapse;padding:2px">' \
                                          '<tr>' \
                                          '<td>Fundo: </td>' \
                                          '<td>' \
                                          + p.alias + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Lote: </td>' \
                                          '<td> ' \
                                          + cod + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Fecha: </td>' \
                                          '<td> ' \
                                          + fechaf + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Hilera: </td>' \
                                          '<td> Del ' \
                                          + str(p_hi) + \
                                          ' al ' \
                                          + str(p_hf) + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Ha Cosechada: </td>' \
                                          '<td> ' \
                                          + str(p_hc) + \
                                          ' ha</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Variedad: </td>' \
                                          '<td> ' \
                                          + variedad + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Optimo: </td>' \
                                          '<td> ' \
                                          + str(optimo) + ' d' + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Real: </td>' \
                                          '<td> ' \
                                          + str(dias_rest) + ' d' + \
                                          '</td>' \
                                          '</tr>' \
                                          '</table>' \
                                          '</td>' \
                                          '</tr>' \
                                          '</table>' \
                                          '</body>' \
                                          '</html>'

                        pol.outerboundaryis = lineas
                        pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
                        pol.style.linestyle.width = 1
                        pol.style.polystyle.color = simplekml.Color.changealphaint(100, simplekml.Color.rgb(r, g, b))

                        # print(fechaf + ' ' + cod + ' ' + hi + ' ' + hf)
                        parame = []
                        lineas = []

                if total > 0:
                    ff_hoy = str(date.today().year) + str(date.today().month) + str(date.today().day)
                    if not os.path.exists(
                            'static/datos/pre_cosecha/' + str(f_id) + '/' + str(e_id) + '/' + str(c_id) + '/' + ff_hoy):
                        os.makedirs(
                            'static/datos/pre_cosecha/' + str(f_id) + '/' + str(e_id) + '/' + str(c_id) + '/' + ff_hoy)

                    url = 'static/datos/pre_cosecha/' + str(f_id) + '/' + str(e_id) + '/' + str(
                        c_id) + '/' + ff_hoy + '/kml_pre_cosecha_' + ff_hoy + "_" + hora_f + '.kmz'
                    kml.savekmz(url, format=False)

                else:
                    url = 0

                return JsonResponse([url, [str(a4) + ' ha', str(a3) + ' ha', str(a2) + ' ha', str(a1) + ' ha',
                                           str(xx) + ' ha', str(b1) + ' ha', str(b2) + ' ha', str(b3) + ' ha',
                                           str(b4) + ' ha', 'Total: ' + str(tot_ley) + ' ha', str(a5) + ' ha',
                                           str(a6) + ' ha', str(a7) + ' ha', str(a8) + ' ha'], solo_lineas], safe=False)
            else:
                return Response([])
    else:
        return Response([2])


@api_view(['GET'])
def v_pasadas(request):
    if request.user.is_authenticated:
        print(request.user.username + " consultó pasadas.")
        if request.method == 'GET':
            api = request.GET['api']
            d = datetime.datetime.now().day
            m = datetime.datetime.now().month
            a = datetime.datetime.now().year
            key = "86d20cd037d5bd84611248f47c3a021e1c1c1310"
            data = key + str(d) + str(m) + str(a)
            sdft = hmac.new(
                "Margito".encode('utf-8'),
                data.encode('utf-8'),
                hashlib.sha256
            ).hexdigest()

            hora_f = datetime.datetime.now().strftime('%H%M%S')

            if api == sdft:
                c_id = int(request.GET['c_id'])

                if c_id != 0:
                    queryset = CosechaLotes.objects.raw(
                        'select cl.id, af.alias, al.cod, al.has_sembrado, al.total_hileras, cl.fecha, alh.num_hilera, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, acv.descrip, au.dia, au.n_dia, au.r, au.g, au.b, ROW_NUMBER() over (PARTITION BY al.id, alh.num_hilera order by al.id, cl.fecha, alh.num_hilera) as xd from cosecha_lotes cl inner join adm_umbrales au on au.id = cl.adm_umbrales_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_lotes_hileras alh on al.id = alh.adm_lotes_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id where ac.id = %s and alh.num_hilera BETWEEN cl.hi and cl.hf and alh.adm_cultivos_variedad_id = acv.id order by xd, al.id, alh.num_hilera;',
                        [c_id]
                    )

                pasada, x, vr = 0, 0, ''
                lista = []
                parame = []
                lineas = []
                lu, ma, mi, ju, vi, sa, do, lmmjvsd, z = 0, 0, 0, 0, 0, 0, 0, 0, 0

                total = len(queryset)
                kml = simplekml.Kml()

                for pas in queryset:
                    if x != 0:
                        if vr != pas.id:
                            pol = kml.newpolygon(name='Agrovision')
                            t_p = len(parame) * 2
                            p_hi = parame[0][4]
                            p_hf = parame[len(parame) - 1][4]
                            p_hc = round(((p_hf - p_hi + 1) * has_sembrado) / tot_hileras, 2)

                            pol.description = '<!<html xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:msxsl="urn:schemas-microsoft-com:xslt">' \
                                              '<head>' \
                                              '<meta charset="UTF-8">' \
                                              '<title>Title</title>' \
                                              '</head>' \
                                              '<body style="margin:0px 0px 0px 0px;overflow:auto;background:#FFFFFF;height: auto">' \
                                              '<table style="font-family:Arial,Verdana,Times;font-size:15px;text-align:left;width:100%;height:50%;border-collapse:collapse;padding:2px">' \
                                              '<tr>' \
                                              '<td>Fundo: </td>' \
                                              '<td>' + pas.alias + '</td>' \
                                                                   '</tr>' \
                                                                   '<tr bgcolor="#D4E4F3">' \
                                                                   '<td>Lote: </td>' \
                                                                   '<td> ' \
                                              + str(cod) + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Fecha: </td>' \
                                              '<td> ' \
                                              + str(fechaf) + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr  bgcolor="#D4E4F3">' \
                                              '<td>Dia: </td>' \
                                              '<td> ' \
                                              + str(dia) + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Hilera: </td>' \
                                              '<td> Del ' \
                                              + str(p_hi) + \
                                              ' al ' \
                                              + str(p_hf) + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Ha Cosechada: </td>' \
                                              '<td> ' \
                                              + str(p_hc) + \
                                              ' ha</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Variedad: </td>' \
                                              '<td> ' \
                                              + str(variedad) + \
                                              '</td>' \
                                              '</tr>' \
                                              '</table>' \
                                              '</td>' \
                                              '</tr>' \
                                              '</table>' \
                                              '</body>' \
                                              '</html>'

                            for y in range(0, t_p):
                                if y >= len(parame):
                                    c = y - (z + 1) - z
                                    z += 1
                                    lineas.append((parame[c][3], parame[c][2]))

                                else:
                                    c = y
                                    z = 0
                                    lineas.append((parame[c][1], parame[c][0]))

                            lineas.append((parame[0][1], parame[0][0]))
                            pol.outerboundaryis = lineas
                            pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
                            pol.style.linestyle.width = 1
                            pol.style.polystyle.color = simplekml.Color.changealphaint(100,
                                                                                       simplekml.Color.rgb(r, g, b))

                            parame = []
                            lineas = []

                            if n_dia == 1:
                                lu += p_hc
                            elif n_dia == 2:
                                ma += p_hc
                            elif n_dia == 3:
                                mi += p_hc
                            elif n_dia == 4:
                                ju += p_hc
                            elif n_dia == 5:
                                vi += p_hc
                            elif n_dia == 6:
                                sa += p_hc
                            elif n_dia == 7:
                                do += p_hc

                            lmmjvsd += p_hc

                        if pasada != pas.xd:
                            ff_hoy = str(date.today().year) + str(date.today().month) + str(date.today().day)
                            if not os.path.exists('static/datos/pasadas/' + str(c_id) + '/' + ff_hoy):
                                os.makedirs('static/datos/pasadas/' + str(c_id) + '/' + ff_hoy)

                            url = 'static/datos/pasadas/' + str(
                                c_id) + '/' + ff_hoy + '/kml_pasadas_' + ff_hoy + "_" + hora_f + "_" + str(
                                pasada) + '.kmz'
                            kml.savekmz(url, format=False)

                            lista.append([pasada, 'Pasada ' + str(pasada), url,
                                          [str(lu) + ' ha', str(ma) + ' ha', str(mi) + ' ha', str(ju) + ' ha',
                                           str(vi) + ' ha', str(sa) + ' ha', str(do) + ' ha',
                                           'Total: ' + str(lmmjvsd) + ' ha']])
                            lu, ma, mi, ju, vi, sa, do, lmmjvsd, z = 0, 0, 0, 0, 0, 0, 0, 0, 0

                            kml = simplekml.Kml()

                    fechasf = datetime.datetime.strptime(str(pas.fecha), '%Y-%m-%d')
                    fechaf = str(fechasf.strftime('%d/%m/%Y'))
                    cod = pas.cod
                    variedad = pas.descrip
                    has_sembrado = pas.has_sembrado
                    tot_hileras = pas.total_hileras
                    dia = pas.dia
                    n_dia = pas.n_dia
                    r, g, b = pas.r, pas.g, pas.b

                    parame.append([pas.latitud1, pas.longitud1, pas.latitud2, pas.longitud2, pas.num_hilera])

                    x += 1
                    pasada = pas.xd
                    vr = pas.id

                    if x == total:
                        pol = kml.newpolygon(name='Agrovision')
                        t_p = len(parame) * 2
                        p_hi = parame[0][4]
                        p_hf = parame[len(parame) - 1][4]
                        p_hc = round(((p_hf - p_hi + 1) * has_sembrado) / tot_hileras, 2)

                        pol.description = '' \
                                          '<!<html xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:msxsl="urn:schemas-microsoft-com:xslt">' \
                                          '<head>' \
                                          '<meta charset="UTF-8">' \
                                          '<title>Title</title>' \
                                          '</head>' \
                                          '<body style="margin:0px 0px 0px 0px;overflow:auto;background:#FFFFFF;height: auto">' \
                                          '<table style="font-family:Arial,Verdana,Times;font-size:15px;text-align:left;width:100%;height:50%;border-collapse:collapse;padding:2px">' \
                                          '<tr>' \
                                          '<td>Fundo: </td>' \
                                          '<td>' \
                                          + pas.alias + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Lote: </td>' \
                                          '<td> ' \
                                          + str(cod) + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Fecha: </td>' \
                                          '<td> ' \
                                          + str(fechaf) + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr  bgcolor="#D4E4F3">' \
                                          '<td>Dia: </td>' \
                                          '<td> ' \
                                          + str(dia) + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Hilera: </td>' \
                                          '<td> Del ' \
                                          + str(p_hi) + \
                                          ' al ' \
                                          + str(p_hf) + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Ha Cosechada: </td>' \
                                          '<td> ' \
                                          + str(p_hc) + \
                                          ' ha</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Variedad: </td>' \
                                          '<td> ' \
                                          + str(variedad) + \
                                          '</td>' \
                                          '</tr>' \
                                          '</table>' \
                                          '</td>' \
                                          '</tr>' \
                                          '</table>' \
                                          '</body>' \
                                          '</html>'

                        for y in range(0, t_p):
                            if y >= len(parame):
                                c = y - (z + 1) - z
                                z += 1
                                lineas.append((parame[c][3], parame[c][2]))

                            else:
                                c = y
                                z = 0
                                lineas.append((parame[c][1], parame[c][0]))

                        lineas.append((parame[0][1], parame[0][0]))

                        lineas.append((parame[0][1], parame[0][0]))
                        pol.outerboundaryis = lineas
                        pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
                        pol.style.linestyle.width = 1
                        pol.style.polystyle.color = simplekml.Color.changealphaint(100, simplekml.Color.rgb(r, g, b))

                        parame = []
                        lineas = []

                        if n_dia == 1:
                            lu += p_hc
                        elif n_dia == 2:
                            ma += p_hc
                        elif n_dia == 3:
                            mi += p_hc
                        elif n_dia == 4:
                            ju += p_hc
                        elif n_dia == 5:
                            vi += p_hc
                        elif n_dia == 6:
                            sa += p_hc
                        elif n_dia == 7:
                            do += p_hc

                        lmmjvsd += p_hc

                        ff_hoy = str(date.today().year) + str(date.today().month) + str(date.today().day)
                        if not os.path.exists('static/datos/pasadas/' + str(c_id) + '/' + ff_hoy):
                            os.makedirs('static/datos/pasadas/' + str(c_id) + '/' + ff_hoy)

                        url = 'static/datos/pasadas/' + str(
                            c_id) + '/' + ff_hoy + '/kml_pasadas_' + ff_hoy + "_" + hora_f + "_" + str(pasada) + '.kmz'
                        kml.savekmz(url, format=False)

                        lista.append([pasada, 'Pasada ' + str(pasada), url,
                                      [str(lu) + ' ha', str(ma) + ' ha', str(mi) + ' ha', str(ju) + ' ha',
                                       str(vi) + ' ha', str(sa) + ' ha', str(do) + ' ha',
                                       'Total: ' + str(lmmjvsd) + ' ha']])

                if x != 0:
                    return JsonResponse(["lista", lista], safe=False)
                else:
                    return Response([0])
                # return JsonResponse([url, [str(a4)+' ha', str(a3)+' ha', str(a2)+' ha', str(a1)+' ha', str(xx)+' ha', str(b1)+' ha', str(b2)+' ha', str(b3)+' ha', str(b4)+' ha', 'Total: '+str(tot_ley)+' ha']], safe=False)
            else:
                return Response([])
    else:
        return Response([2])


@api_view(['GET'])
def v_pre_cosecha_sanidad(request):
    if request.user.is_authenticated:
        print(request.user.username + " consultó pre cosecha.")
        if request.method == 'GET':
            api = request.GET['api']
            d = datetime.datetime.now().day
            m = datetime.datetime.now().month
            a = datetime.datetime.now().year
            key = "86d20cd037d5bd84611248f47c3a021e1c1c1310"
            data = key + str(d) + str(m) + str(a)
            sdft = hmac.new(
                "Margito".encode('utf-8'),
                data.encode('utf-8'),
                hashlib.sha256
            ).hexdigest()

            hora_f = datetime.datetime.now().strftime('%H%M%S')

            if api == sdft:
                f_id = int(request.GET['f_id'])
                e_id = int(request.GET['e_id'])
                c_id = int(request.GET['c_id'])
                v_id = int(request.GET['v_id'])
                v_dias = int(request.GET['v_dias'])
                fecha_i = datetime.datetime.strptime(str(request.GET['fechaf']), '%Y-%m-%d')

                if v_id != 0:
                    queryset = AdmLotesHileras.objects.raw(
                        # 'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id = %s and ac.id = %s and acv.id = %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado = 0 group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by al.cod, alh.num_hilera;',
                        'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado, IIF(DATEDIFF(day, max(cl.fecha), %s) <= (cv.dias - 3), 0, IIF(DATEDIFF(day, max(cl.fecha), %s) = (cv.dias - 2), 1, IIF(DATEDIFF(day, max(cl.fecha), %s) >= (cv.dias - 1), 2, 5))) as estado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id = %s and ac.id = %s and acv.id = %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado = 0 group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by estado, al.cod, alh.num_hilera;',
                        [fecha_i, fecha_i, fecha_i, f_id, e_id, c_id, v_id, fecha_i]
                    )
                elif c_id != 0 and v_id == 0:
                    queryset = AdmLotesHileras.objects.raw(
                        # 'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id = %s and ac.id = %s and acv.id != %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado = 0 group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by al.cod, alh.num_hilera;',
                        'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado, IIF(DATEDIFF(day, max(cl.fecha), %s) <= (cv.dias - 3), 0, IIF(DATEDIFF(day, max(cl.fecha), %s) = (cv.dias - 2), 1, IIF(DATEDIFF(day, max(cl.fecha), %s) >= (cv.dias - 1), 2, 5))) as estado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id = %s and ac.id = %s and acv.id != %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado = 0 group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by estado, al.cod, alh.num_hilera;',
                        [fecha_i, fecha_i, fecha_i, f_id, e_id, c_id, v_id, fecha_i]
                    )
                elif e_id != 0 and c_id == 0 and v_id == 0:
                    queryset = AdmLotesHileras.objects.raw(
                        # 'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id = %s and ac.id != %s and acv.id != %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado = 0 group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by al.cod, alh.num_hilera;',
                        'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado, IIF(DATEDIFF(day, max(cl.fecha), %s) <= (cv.dias - 3), 0, IIF(DATEDIFF(day, max(cl.fecha), %s) = (cv.dias - 2), 1, IIF(DATEDIFF(day, max(cl.fecha), %s) >= (cv.dias - 1), 2, 5))) as estado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id = %s and ac.id != %s and acv.id != %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado = 0 group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by estado, al.cod, alh.num_hilera;',
                        [fecha_i, fecha_i, fecha_i, f_id, e_id, c_id, v_id, fecha_i]
                    )
                elif f_id != 0 and e_id == 0 and c_id == 0 and v_id == 0:
                    queryset = AdmLotesHileras.objects.raw(
                        # 'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id != %s and ac.id != %s and acv.id != %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado = 0 group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by al.cod, alh.num_hilera;',
                        'select alh.id, af.alias, al.cod, max(cl.fecha) as fecha, acv.descrip, count(alh.num_hilera) as n, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado, IIF(DATEDIFF(day, max(cl.fecha), %s) <= (cv.dias - 3), 0, IIF(DATEDIFF(day, max(cl.fecha), %s) = (cv.dias - 2), 1, IIF(DATEDIFF(day, max(cl.fecha), %s) >= (cv.dias - 1), 2, 5))) as estado from adm_lotes_hileras alh inner join cosecha_lotes cl on alh.adm_lotes_id = cl.adm_lotes_id inner join adm_lotes al on al.id = cl.adm_lotes_id inner join adm_turnos at on at.id = al.adm_turnos_id inner join adm_campos ac on ac.id = at.adm_campos_id inner join adm_etapas ae on ae.id = ac.adm_etapas_id inner join adm_fundos af on af.id = ae.adm_fundos_id inner join adm_cultivos_variedad acv on acv.id = alh.adm_cultivos_variedad_id inner join cosecha_vigencias cv on ac.id = cv.adm_campos_id where af.id = %s and ae.id != %s and ac.id != %s and acv.id != %s and cl.fecha <= %s and alh.num_hilera BETWEEN cl.hi and cl.hf and cv.adm_cultivos_variedad_id = alh.adm_cultivos_variedad_id and cv.estado = 0 and alh.estado = 0 and cl.estado = 0 group by alh.id, af.alias, al.cod, acv.descrip, alh.num_hilera, cv.dias, alh.latitud1, alh.longitud1, alh.latitud2, alh.longitud2, al.total_hileras, al.has_sembrado order by estado, al.cod, alh.num_hilera;',
                        [fecha_i, fecha_i, fecha_i, f_id, e_id, c_id, v_id, fecha_i]
                    )

                x, z, vr = 0, 0, ''
                parame = []
                lineas = []
                a4, a3, a2, a1, xx, b1, b2, b3, b4, tot_ley = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
                kml = simplekml.Kml()
                r = random.randrange(0, 255)
                g = random.randrange(0, 255)
                b = random.randrange(0, 255)

                cds = [[255, 255, 0], [255, 150, 0], [255, 0, 0]]
                total = len(queryset)

                for p in queryset:
                    if x != 0:
                        if vr != str(p.estado) + p.cod + str(p.descrip) + str(p.num_hilera - 1):
                            pol = kml.newpolygon(name='Agrovision')

                            t_p = len(parame) * 2

                            p_hi = parame[0][4]
                            p_hf = parame[len(parame) - 1][4]

                            p_hc = round(((p_hf - p_hi + 1) * has_sembrado) / tot_hileras, 2)
                            dias_rest = (fecha_i - fechasf).days

                            for y in range(0, t_p):
                                if y >= len(parame):
                                    c = y - (z + 1) - z
                                    z += 1
                                    lineas.append((parame[c][3], parame[c][2]))

                                else:
                                    c = y
                                    z = 0
                                    lineas.append((parame[c][1], parame[c][0]))

                            lineas.append((parame[0][1], parame[0][0]))

                            if estad == 0:
                                r = cds[0][0]
                                g = cds[0][1]
                                b = cds[0][2]
                                a4 += p_hc

                            elif estad == 1:
                                r = cds[1][0]
                                g = cds[1][1]
                                b = cds[1][2]
                                a3 += p_hc

                            elif estad == 2:
                                r = cds[2][0]
                                g = cds[2][1]
                                b = cds[2][2]
                                a2 += p_hc

                            tot_ley += p_hc

                            pol.description = '' \
                                              '<!<html xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:msxsl="urn:schemas-microsoft-com:xslt">' \
                                              '<head>' \
                                              '<meta charset="UTF-8">' \
                                              '<title>Title</title>' \
                                              '</head>' \
                                              '<body style="margin:0px 0px 0px 0px;overflow:auto;background:#FFFFFF;height: auto">' \
                                              '<table style="font-family:Arial,Verdana,Times;font-size:15px;text-align:left;width:100%;height:50%;border-collapse:collapse;padding:2px">' \
                                              '<tr>' \
                                              '<td>Fundo: </td>' \
                                              '<td>' \
                                              + p.alias + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Lote: </td>' \
                                              '<td> ' \
                                              + cod + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Fecha: </td>' \
                                              '<td> ' \
                                              + fechaf + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Hilera: </td>' \
                                              '<td> Del ' \
                                              + str(p_hi) + \
                                              ' al ' \
                                              + str(p_hf) + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Ha Cosechada: </td>' \
                                              '<td> ' \
                                              + str(p_hc) + \
                                              ' ha</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Variedad: </td>' \
                                              '<td> ' \
                                              + variedad + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Optimo: </td>' \
                                              '<td> ' \
                                              + str(optimo) + ' d' + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr bgcolor="#D4E4F3">' \
                                              '<td>Real: </td>' \
                                              '<td> ' \
                                              + str(dias_rest) + ' d' + \
                                              '</td>' \
                                              '</tr>' \
                                              '<tr>' \
                                              '<td>Ultima Pasada: </td>' \
                                              '<td> ' \
                                              + str(pasada) + \
                                              '</td>' \
                                              '</tr>' \
                                              '</table>' \
                                              '</td>' \
                                              '</tr>' \
                                              '</table>' \
                                              '</body>' \
                                              '</html>'

                            pol.outerboundaryis = lineas
                            pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
                            pol.style.linestyle.width = 1
                            pol.style.polystyle.color = simplekml.Color.changealphaint(100,
                                                                                       simplekml.Color.rgb(r, g, b))

                            # print(fechaf + ' ' + cod + ' ' + hi + ' ' + hf)
                            parame = []
                            lineas = []

                    fechasf = datetime.datetime.strptime(str(p.fecha), '%Y-%m-%d')
                    fechaf = str(fechasf.strftime('%d/%m/%Y'))
                    cod = p.cod
                    variedad = p.descrip
                    has_sembrado = p.has_sembrado
                    tot_hileras = p.total_hileras
                    pasada = p.n
                    optimo = p.dias
                    estad = p.estado

                    parame.append([p.latitud1, p.longitud1, p.latitud2, p.longitud2, p.num_hilera])

                    x += 1
                    vr = str(p.estado) + p.cod + str(p.descrip) + str(p.num_hilera)

                    if x == total:
                        pol = kml.newpolygon(name='Agrovision')

                        t_p = len(parame) * 2

                        p_hi = parame[0][4]
                        p_hf = parame[len(parame) - 1][4]

                        p_hc = round(((p_hf - p_hi + 1) * has_sembrado) / tot_hileras, 2)
                        dias_rest = (fecha_i - fechasf).days

                        for y in range(0, t_p):
                            if y >= len(parame):
                                c = y - (z + 1) - z
                                z += 1
                                lineas.append((parame[c][3], parame[c][2]))

                            else:
                                c = y
                                z = 0
                                lineas.append((parame[c][1], parame[c][0]))

                        lineas.append((parame[0][1], parame[0][0]))

                        if estad == 0:
                            r = cds[0][0]
                            g = cds[0][1]
                            b = cds[0][2]
                            a4 += p_hc

                        elif estad == 1:
                            r = cds[1][0]
                            g = cds[1][1]
                            b = cds[1][2]
                            a3 += p_hc

                        elif estad == 2:
                            r = cds[2][0]
                            g = cds[2][1]
                            b = cds[2][2]
                            a2 += p_hc

                        tot_ley += p_hc

                        pol.description = '' \
                                          '<!<html xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:msxsl="urn:schemas-microsoft-com:xslt">' \
                                          '<head>' \
                                          '<meta charset="UTF-8">' \
                                          '<title>Title</title>' \
                                          '</head>' \
                                          '<body style="margin:0px 0px 0px 0px;overflow:auto;background:#FFFFFF;height: auto">' \
                                          '<table style="font-family:Arial,Verdana,Times;font-size:15px;text-align:left;width:100%;height:50%;border-collapse:collapse;padding:2px">' \
                                          '<tr>' \
                                          '<td>Fundo: </td>' \
                                          '<td>' \
                                          + p.alias + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Lote: </td>' \
                                          '<td> ' \
                                          + cod + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Fecha: </td>' \
                                          '<td> ' \
                                          + fechaf + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Hilera: </td>' \
                                          '<td> Del ' \
                                          + str(p_hi) + \
                                          ' al ' \
                                          + str(p_hf) + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Ha Cosechada: </td>' \
                                          '<td> ' \
                                          + str(p_hc) + \
                                          ' ha</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Variedad: </td>' \
                                          '<td> ' \
                                          + variedad + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Optimo: </td>' \
                                          '<td> ' \
                                          + str(optimo) + ' d' + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr bgcolor="#D4E4F3">' \
                                          '<td>Real: </td>' \
                                          '<td> ' \
                                          + str(dias_rest) + ' d' + \
                                          '</td>' \
                                          '</tr>' \
                                          '<tr>' \
                                          '<td>Ultima Pasada: </td>' \
                                          '<td> ' \
                                          + str(pasada) + \
                                          '</td>' \
                                          '</tr>' \
                                          '</table>' \
                                          '</td>' \
                                          '</tr>' \
                                          '</table>' \
                                          '</body>' \
                                          '</html>'

                        pol.outerboundaryis = lineas
                        pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
                        pol.style.linestyle.width = 1
                        pol.style.polystyle.color = simplekml.Color.changealphaint(100, simplekml.Color.rgb(r, g, b))
                        parame = []
                        lineas = []

                if total > 0:
                    ff_hoy = str(date.today().year) + str(date.today().month) + str(date.today().day)
                    if not os.path.exists('static/datos/pre_cosecha_sanidad/' + str(f_id) + '/' + str(e_id) + '/' + str(
                            c_id) + '/' + ff_hoy):
                        os.makedirs('static/datos/pre_cosecha_sanidad/' + str(f_id) + '/' + str(e_id) + '/' + str(
                            c_id) + '/' + ff_hoy)

                    url = 'static/datos/pre_cosecha_sanidad/' + str(f_id) + '/' + str(e_id) + '/' + str(
                        c_id) + '/' + ff_hoy + '/kml_pre_cosecha_sanidad_' + ff_hoy + "_" + hora_f + '.kmz'
                    kml.savekmz(url, format=False)

                else:
                    url = 0

                return JsonResponse([url, [str(a4) + ' ha', str(a3) + ' ha', str(a2) + ' ha', str(a1) + ' ha',
                                           str(xx) + ' ha', str(b1) + ' ha', str(b2) + ' ha', str(b3) + ' ha',
                                           str(b4) + ' ha', 'Total: ' + str(tot_ley) + ' ha']], safe=False)
            else:
                return Response([])
    else:
        return Response([2])


class Login(FormView):
    template_name = 'loginb.html'
    form_class = FormularioLogin
    success_url = reverse_lazy('inicio')

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(Login, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(Login, self).form_valid(form)


def logoutUsuario(request):
    print(request.user.username + " cerró sesión.")
    logout(request)
    return HttpResponseRedirect('/accounts/login/')
