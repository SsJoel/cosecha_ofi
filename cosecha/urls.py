from _ast import List

from django.urls import path
from django.conf.urls import url
from cosecha.views import home_view
from rest_framework.routers import DefaultRouter

from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token


router = DefaultRouter()
# router.register(r'fecv', ListarFe, basename='fecv')


urlpatterns = [
    path('', home_view, name='home'),

    # path('fecv/', ListarFe.as_view(), name='fecv'),
    # url(r'^fecv$', ListarFe, name='fecv')
    # path('colo/', ListarCosechaLotes.as_view(), name='colo')
]
