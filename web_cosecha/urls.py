from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.authtoken.views import obtain_auth_token
from django.contrib.auth.decorators import login_required

from cosecha.views import home_view, v_fundos, v_cosecha, v_pre_cosecha, Login, logoutUsuario, api_etapas, api_lotes, v_pasadas, api_fundos, v_pre_cosecha_sanidad, api_umbrales, api_evaluadores, api_variedades, api_ctlv, api_cosecha_lotes, api_fe, api_ctlv_b, api_covi, api_campana, api_cosecha_sem

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', login_required(home_view), name='inicio'),
    path('accounts/login/', Login.as_view(), name="login"),
    path('logout/', login_required(logoutUsuario), name="logout"),
    #path('api-token-auth/', obtain_auth_token, name='api-token-auth'),
    path('fundos/', login_required(v_fundos), name='fundos'),
    path('cosecha/', login_required(v_cosecha), name='cosecha'),
    path('pre_cosecha/', login_required(v_pre_cosecha), name='pre_cosecha'),
    path('pasadas/', login_required(v_pasadas), name='pasadas'),
    path('pre_cosecha_sanidad/', login_required(v_pre_cosecha_sanidad), name='sanidad'),
    path('api_fe/', api_fe, name='api_fe'),
    path('api_campana/', api_campana, name='api_campana'),
    path('api_fundos/', api_fundos, name='api_fundos'),
    path('api_etapas/', api_etapas, name='api_etapas'),
    path('api_lotes/', api_ctlv, name='api_lotes'),
    path('api_lotes_b/', api_ctlv_b, name='api_lotes_b'),
    path('api_umbrales/', api_umbrales, name='api_umbrales'),
    path('api_evaluadores/', api_evaluadores, name='api_evaluadores'),
    path('api_variedades/', api_variedades, name='api_variedades'),
    path('api_cosecha_lotes/', api_cosecha_lotes, name='api_cosecha_lotes'),
    path('api_covi/', api_covi, name='api_covi'),
    path('api_cosecha_sem/', api_cosecha_sem, name='api_cosecha_sem'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
